import 'package:hooks_riverpod/hooks_riverpod.dart';
import 'package:isar/isar.dart';
import 'package:riverpod_mvvm/src/services/client.service.dart';
import 'package:riverpod_mvvm/src/shared/client.dart';

class ClientsNotifier extends StateNotifier<List<Client>> {
  ClientsNotifier(this._repository) : super([]);
  final ClientService _repository;

  Future<void> add(Client client) async {
    try {
      await _repository.insert(client);
      state = [...state, client];
    } catch (_) {
      rethrow;
    }
  }

  void update(Client client) {
    // replace client in state with updated client
    state = state.map((c) => c.id == client.id ? client : c).toList();
    _repository.update(client);
  }

  void delete(Id id) {
    state = state.where((c) => c.id != id).toList();
    _repository.delete(id);
  }
}

final clientsProvider = StateNotifierProvider<ClientsNotifier, List<Client>>(
  (ref) => ClientsNotifier(ClientService.instance),
);

final clientFutureProvider = FutureProvider<List<Client>>((ref) async {
  final client = ref.watch(clientsProvider);
  if (client.isEmpty) {
    client.addAll(await ClientService.instance.getAllForClient('1'));
  }
  return client;
});

class ClientDetailNotifier extends StateNotifier<Client> {
  ClientDetailNotifier() : super(const Client(id: 0));
  void setDetails(Client client) {
    state = state.copyWith(
      id: client.id,
      name: client.name,
      email: client.email,
      phone: client.phone,
      address: client.address,
      city: client.city,
      state: client.state,
      zip: client.zip,
      country: client.country,
      notes: client.notes,
      userId: client.userId,
      createdAt: client.createdAt,
      updatedAt: client.updatedAt,
    );
  }
}

final clientDetailProvider = StateNotifierProvider<ClientDetailNotifier, Client>((ref) => ClientDetailNotifier());
