import 'package:flutter/material.dart';
import 'package:riverpod_mvvm/src/screens/details/details_screen.dart';
import 'package:riverpod_mvvm/src/screens/home/home_screen.dart';

class Routes {
  static const home = '/';
  static const details = '/details';
  static Route<dynamic> generateRoute(RouteSettings settings) {
    switch (settings.name) {
      case home:
        return MaterialPageRoute(builder: (_) => const HomeScreen());
      case details:
        return MaterialPageRoute(builder: (_) => const DetailsScreen());
      default:
        return MaterialPageRoute(builder: (_) => const HomeScreen());
    }
  }
}
