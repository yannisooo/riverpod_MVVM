import 'package:flutter/material.dart';
import 'package:hooks_riverpod/hooks_riverpod.dart';
import 'package:riverpod_mvvm/src/core/ui/molecules/custom_scaffold.dart';
import 'package:riverpod_mvvm/src/providers/client.provider.dart';

class DetailsScreen extends ConsumerWidget {
  const DetailsScreen({super.key});

  @override
  Widget build(BuildContext context, WidgetRef ref) {
    final client = ref.watch(clientDetailProvider);
    return CustomScaffold(
      title: client.name,
      floatingActionButton: IconButton(
        icon: const Icon(Icons.arrow_back_ios),
        onPressed: () {
          Navigator.of(context).pop();
        },
      ),
      body: Column(
        children: [
          Text(client.address ?? ''),
          Text(client.city ?? ''),
          Text(client.state ?? ''),
          Text(client.zip ?? ''),
          Text(client.country ?? ''),
        ],
      ),
    );
  }
}
