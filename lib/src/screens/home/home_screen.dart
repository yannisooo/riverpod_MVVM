import 'package:flutter/material.dart';
import 'package:hooks_riverpod/hooks_riverpod.dart';
import 'package:riverpod_mvvm/src/core/constants/custom_colors.dart';
import 'package:riverpod_mvvm/src/core/ui/molecules/custom_scaffold.dart';
import 'package:riverpod_mvvm/src/providers/client.provider.dart';
import 'package:riverpod_mvvm/src/router.dart';
import 'package:riverpod_mvvm/src/shared/client.dart';

class HomeScreen extends HookConsumerWidget {
  const HomeScreen({super.key});

  @override
  Widget build(BuildContext context, WidgetRef ref) {
    final client = ref.watch(clientFutureProvider);
    final clientNotifier = ref.watch(clientsProvider.notifier);

    return CustomScaffold(
      title: 'Home',
      floatingActionButton: client.when(
        data: (data) {
          return IconButton(
            icon: const Icon(Icons.add),
            onPressed: () {
              clientNotifier.add(
                Client(
                  id: data.length + 1,
                  name: 'Test',
                  userId: '1',
                ),
              );
            },
          );
        },
        loading: () => const CircularProgressIndicator(),
        error: (error, stack) => const SizedBox(),
      ),
      body: client.when(
        data: (data) {
          return ListView.builder(
            itemCount: data.length,
            itemBuilder: (context, index) {
              final client = data[index];
              return InkWell(
                onTap: () {
                  ref.read(clientDetailProvider.notifier).setDetails(client);
                  Navigator.of(context).pushNamed(Routes.details);
                },
                child: ListTile(
                  title: Text(client.name ?? ''),
                  trailing: IconButton(
                    icon: const Icon(Icons.delete),
                    onPressed: () {
                      clientNotifier.delete(client.id);
                    },
                  ),
                ),
              );
            },
          );
        },
        loading: () => const Center(
          child: CircularProgressIndicator(
            color: CustomColors.primary,
          ),
        ),
        error: (error, stack) => const Center(
          child: Text('Error'),
        ),
      ),
    );
  }
}
