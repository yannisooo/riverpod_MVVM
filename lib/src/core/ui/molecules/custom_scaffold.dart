import 'package:flutter/material.dart';
import 'package:riverpod_mvvm/src/core/constants/custom_colors.dart';

class CustomScaffold extends StatelessWidget {
  const CustomScaffold({
    super.key,
    required this.body,
    this.floatingActionButton,
    this.title,
    this.backgroundColor = CustomColors.white,
  });
  final Widget body;
  final Widget? floatingActionButton;
  final String? title;
  final Color backgroundColor;

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        title: Text(title ?? ''),
      ),
      backgroundColor: backgroundColor,
      floatingActionButton: floatingActionButton,
      body: body,
    );
  }
}
