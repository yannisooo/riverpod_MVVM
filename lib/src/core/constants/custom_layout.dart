class LayoutConstants {
  // Responsive breakpoints
  /// Desktop screen >= 900px
  static const double screenDesktop = 900;

  // Spacing
  /// Space Zero = 0.0
  static const double spaceZero = 0;

  /// Space XS = 2.0
  static const double spaceXS = 2;

  /// Space S = 4.0
  static const double spaceS = 4;

  /// Space M = 8.0
  static const double spaceM = 8;

  /// Space L = 16.0
  static const double spaceL = 16;

  /// Space XL = 32.0
  static const double spaceXL = 32;
}
