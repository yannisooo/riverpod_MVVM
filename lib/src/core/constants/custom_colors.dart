import 'package:flutter/material.dart';

class CustomColors {
  static const Color primary = Color(0xFFF9FBFD);
  static const Color secondary = Color(0xFFE5E5E5);
  static const Color tertiary = Color(0xFFC4C4C4);
  static const Color quaternary = Color(0xFF9B9B9B);
  static const Color quinary = Color(0xFF6E6E6E);
  static const Color error = Color(0xFF4B4B4B);
  static const Color success = Color(0xFF4B4B4B);
  static const Color warning = Color(0xFF4B4B4B);
  static const Color info = Color(0xFF4B4B4B);
  static const Color white = Color(0xFFFFFFFF);
  static const Color black = Color(0xFF000000);
  static const Color transparent = Color(0x00000000);
}
