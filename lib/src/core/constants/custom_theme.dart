import 'package:flutter/material.dart';
import 'package:riverpod_mvvm/src/core/constants/custom_colors.dart';

mixin CustomTheme {
  //Theme applied when the device is in light mode
  static ThemeData get light => ThemeData(
        // textTheme: const TextTheme(
        //     bodyText2: TextStyle(height: 1.5),
        // ),
        primaryColor: CustomColors.primary,
        disabledColor: CustomColors.tertiary,
        errorColor: CustomColors.error,
        bottomSheetTheme: const BottomSheetThemeData(
          backgroundColor: CustomColors.white,
        ),
        colorScheme: const ColorScheme(
          primary: CustomColors.primary,
          secondary: CustomColors.secondary,
          background: CustomColors.white,
          brightness: Brightness.light,
          error: CustomColors.white,
          onBackground: CustomColors.primary,
          onError: CustomColors.error,
          onPrimary: CustomColors.primary,
          onSecondary: CustomColors.secondary,
          onSurface: CustomColors.white,
          surface: CustomColors.primary,
        ),
        dividerColor: CustomColors.tertiary,
        appBarTheme: const AppBarTheme(
          elevation: 0,
          color: CustomColors.primary,
        ),
        tabBarTheme: TabBarTheme(
          labelStyle: h1,
          unselectedLabelStyle: h1,
          unselectedLabelColor: CustomColors.tertiary,
          labelColor: CustomColors.primary,
        ),
        pageTransitionsTheme: const PageTransitionsTheme(
          builders: {
            TargetPlatform.iOS: CupertinoPageTransitionsBuilder(),
            TargetPlatform.android: CupertinoPageTransitionsBuilder(),
          },
        ),
      );

  static TextStyle get h1 => const TextStyle(
        color: CustomColors.black,
        fontFamily: 'Cerebrisans',
        fontWeight: FontWeight.w500,
        fontSize: 30,
      );
}
