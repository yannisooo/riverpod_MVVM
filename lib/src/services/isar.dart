import 'package:isar/isar.dart';
import 'package:riverpod_mvvm/src/shared/client.dart';

class IsarInstances {
  static late final Isar clientInstance;
  static Future<void> init() async {
    clientInstance = await Isar.open([ClientSchema]);
  }
}
