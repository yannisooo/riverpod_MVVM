import 'package:flutter/material.dart';
import 'package:isar/isar.dart';
import 'package:riverpod_mvvm/src/services/isar.dart';
import 'package:riverpod_mvvm/src/shared/client.dart';

@immutable
class ClientService {
  const ClientService._();
  static const instance = ClientService._();
  Future<List<Client>> getAllForClient(String clientId) async =>
      IsarInstances.clientInstance.clients.filter().userIdEqualTo(clientId).findAll();

  Future<Client?> get(int id) async {
    return IsarInstances.clientInstance.clients.get(id);
  }

  Future<void> insert(Client client) async {
    await IsarInstances.clientInstance.writeTxn(() async {
      await IsarInstances.clientInstance.clients.put(client);
    });
  }

  Future<void> update(Client client) async {
    await IsarInstances.clientInstance.writeTxn(() async {
      await IsarInstances.clientInstance.clients.put(client);
    });
  }

  Future<void> delete(int id) async {
    await IsarInstances.clientInstance.writeTxn(() async {
      await IsarInstances.clientInstance.clients.delete(id);
    });
  }
}
